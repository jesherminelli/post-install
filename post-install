#!/usr/bin/env bash
# -----------------------------------------------------------------
# Script      : post-install
# Description : My post instalation
# Version     : 
# Autor       : Jesher Minelli <jesherdevsk8@gmail.com>
# Date        : dom 9 out 2022
# License     : GNU/GPL v3.0
# -----------------------------------------------------------------
# Usage: ./post-install
# -----------------------------------------------------------------

#-------------------------- VARIABLES -----------------------------
NON_ROOT_USER="$(who am i | awk '{print $1}')"
URL_PPA_ULAUNCHER="ppa:agornostal/ulauncher"
FLATHUB="https://flathub.org/repo/flathub.flatpakrepo"
DOWNLOAD_PROGRAMS_DIR="/tmp/deb_programs"
DEB_PROGRAMS=(
  https://dl.google.com/linux/dilsrect/google-chrome-stable_current_amd64.deb
  # Discord
  # slack
)

PACKAGES=(
  build-essential
  vim
  htop
  jq
  wget
  curl
  git
  flatpak
  virtualbox
  guake
  openssh-server
  neofetch
  mariadb-server
  binutils
  cowsay
  gpg
  gparted
  cmatrix
  zsh
  fish
  kazam
  gedit
  flameshot
  debootstrap
  shellcheck
  vagrant
)

FLATPAK_PACKAGES=(
  org.gnome.gitlab.somas.Apostrophe
  com.github.sdv43.whaler
  com.getpostman.Postman
  com.spotify.Client
  org.telegram.desktop
)

# functions for colorized output
# ANSI escape color codes
readonly ansiRed='\e[1;31m'
readonly ansiGreen='\e[1;32m'
readonly ansiYellow='\e[1;33m'
readonly ansiNoColor='\e[0m'

echoRed() {
  echo -e "${ansiRed}$*${ansiNoColor}"
}

echoGreen() {
  echo -e "${ansiGreen}$*${ansiNoColor}"
}

echoYellow() {
  echo -e "${ansiYellow}$*${ansiNoColor}"
}

err() {
  echoRed "$*" >&2
}

warn() {
  echoYellow "$*" >&2
}

#--------------------------- FUNCTIONS -----------------------------
# helper functions
installPkg() {
  sudo apt-get update && sudo apt-get install -y "$@"
}

flatpakInstall() {
  sudo flatpak remote-add --if-not-exists flathub "$FLATHUB"
  sudo flatpak install -y --noninteractive flathub "$@"
}

# returns success if "$1" is installed
isInstalled() {
  local command="$1"
  command -v "${command}" || return 1
  warn "Skipping: '${command}' seems to be already installed."
}

addUserToGroup() {
  sudo usermod -a -G "$@"
}

# add ppa repository
addPpa() {
  sudo add-apt-repository "$URL_PPA_ULAUNCHER" -y &> /dev/null
}

# functions to install software
installVSCode() {
  isInstalled code && return
  echoGreen "\n--> installing Visual Studio Code..."

  # https://code.visualstudio.com/docs/setup/linux#_debian-and-ubuntu-based-distributions
  wget -qO- https://packages.microsoft.com/keys/microsoft.asc \
    | gpg --dearmor > packages.microsoft.gpg
  sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
  rm -f packages.microsoft.gpg

  echo \
    "deb [arch=${ARCHITECTURE} signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" \
    | sudo tee /etc/apt/sources.list.d/vscode.list > /dev/null

  installPkg apt-transport-https
  installPkg code
}

installSublimeText() {
  isInstalled subl && return
  echoGreen "\n--> installing Sublime-text..."

  # https://www.sublimetext.com/docs/linux_repositories.html#apt
  wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor \
    | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg

  echo "deb https://download.sublimetext.com/ apt/stable/" \
    | sudo tee /etc/apt/sources.list.d/sublime-text.list

  installPkg sublime-text
}

installDocker() {
  isInstalled docker && return
  echoGreen "\n--> installing docker..."

  # https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
  installPkg ca-certificates curl gnupg lsb-release

  curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
    | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

  echo \
    "deb [arch=${ARCHITECTURE} signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    ${UBUNTU_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

  installPkg docker-ce docker-ce-cli containerd.io

  addUserToGroup docker "$NON_ROOT_USER"
}

installRvm() {
  isInstalled rvm && return
  echoGreen "\n--> installing RVM..."

  # https://rvm.io/rvm/install
  gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys \
    409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
    curl -sSL https://get.rvm.io | bash

  local BASHRC="$HOME/.bashrc"
  addUserToGroup rvm "$NON_ROOT_USER" && source "$BASHRC"
  # rvm install ruby
}

installUlauncher() {
  isInstalled ulauncher && return
  echoGreen "\n--> installing ulauncher..."

  addPpa
  installPkg ulauncher
}
#---------------------------- MAIN ---------------------------------

main() {
  cd "${HOME}" || return 1

  export ARCHITECTURE="$(dpkg --print-architecture)"

  # get the ubuntu codename
  source /etc/os-release
  if [[ -z "${UBUNTU_CODENAME}" ]]; then
    warn "WARNING: failed to detect UBUNTU_CODENAME while installing docker. Skipping..."
  fi
  export UBUNTU_CODENAME

  installPkg "${PACKAGES[@]}"
  flatpakInstall "${FLATPAK_PACKAGES[@]}"

  # My Dev tools
  installVSCode
  installSublimeText
  installDocker
  installRvm
}

[[ "$0" == "${BASH_SOURCE[0]}" ]] && main "$@"