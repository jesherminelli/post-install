# Change PS1 variable - Bash
PS1='\[\e[1;35m\]\342\224\214\342\224\200\[\e[1;35m\][\[\e[1;36m\]\u\[\e[1;33m\]@\[\e[1;34m\]\h\[\e[1;35m\]]\[\e[1;35m\]\342\224\200\[\e[1;35m\][\[\e[1;32m\]\w\[\e[1;35m\]]\e[1;31m\]$(gitBranch)\[\e[1;35m\]\342\224\200[\[\e[1;37m\]\t\[\e[1;35m\]]\n\[\e[1;35m\]\342\224\224\342\224\200\342\224\200\342\225\274\[\e[1;31m\] \$ \[\e[0m\]'

# another color
PS1='\[\033[01;33m\]\u\[\033[0;32m\]@\[\033[01;36m\]\h\[\033[00m\]\[\033[1;31m\]$(gitBranch)\[\033[00m\]:\[\033[01;37m\]\w\[\033[01;32m\]\$\[\033[00m\] '

# ADD INTO YOUR .bashrc
# if in a git repository, shows the current branch
gitBranch() {
  ret="$?"
  branch="$(git branch --show-current 2> /dev/null)" \
    && echo "(${branch})"
  return "${ret}"
}